import 'package:flutter/foundation.dart';
import 'package:mvvm/repository/repository_memory.dart';
import 'package:mvvm/repository/repository_prefs.dart';
import 'package:mvvm/task_model.dart';

class TaskViewModel extends ChangeNotifier {
  final _repository = RepositoryMemory();

  List<TaskModel> get tasks => _repository.list();
  TaskModel? taskSelected;

  void saveTask(String description) {
    if (taskSelected == null) {
      final id = DateTime.now().microsecondsSinceEpoch.toString();
      _repository.create(TaskModel(description: description, id: id));
    } else {
      _repository.update(taskSelected!);
    }

    notifyListeners();
  }

  void removeTask(TaskModel task) {
    if (task == taskSelected) {
      taskSelected = null;
    }
    _repository.remove(task);

    notifyListeners();
  }

  void setTaskSelected(TaskModel task) {
    taskSelected = task;
    notifyListeners();
  }
}
